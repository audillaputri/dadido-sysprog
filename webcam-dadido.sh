#!/bin/bash

pil=3


echo '============================================ '
echo '     Welcome To Webcam App From DaDiDo'
echo '============================================ '
echo 'keterangan :'
echo 'Default device yang digunakan adalah video0. Cek terlebih dahulu device yang anda gunakan, lalu sesuaikan pada script jika bukan video0'



while true
 do
	echo ' '
	echo ' '
	echo '========================='
	echo ' Main Menu '
	echo '========================='
	echo '1. Tentukan Frame Rate '
	echo '2. Rekam Video (stop dengan ctrl+c )'
	echo '3. Mengubah default ukuran video'
	echo '4. Melihat detail setting webcam V4l2'
	echo '5. Melihat Modinfo'

	echo ' '
	echo 'tekan ctrl+c untuk exit'
	echo ' '

	echo 'Pilih Fitur :'
	read pilihan
	if [[ $pilihan -eq 1 ]]
	then
		echo 'tentukan fps nya (Max = 30) :'
		read num
		sudo v4l2-ctl --set-parm=$num
	elif [[ $pilihan -eq 2 ]]
	then
		echo 'Tulis Nama Output (namanya saja tanpa format) :'
		read name
		if [[ $pil -eq 1 ]]
                then
                        mencoder tv:// -tv driver=v4l2:width=160:height=120:device=/dev/video0 -nosound -ovc lavc -o $name.avi
		elif [[ $pil -eq 2 ]]
                then
                        mencoder tv:// -tv driver=v4l2:width=352:height=288:device=/dev/video0 -nosound -ovc lavc -o $name.avi
		elif [[ $pil -eq 3 ]]
		then
			mencoder tv:// -tv driver=v4l2:width=640:height=480:device=/dev/video0 -nosound -ovc lavc -o $name.avi
		fi

	elif [[ $pilihan -eq 3 ]]
	then
		echo 'Option ukuran :'
		echo '1. 160x120'
		echo '2. 352x288'
		echo '3. 640x480'


		echo ' '
		echo 'Pilih opsi :'
		read pil
		if [[ $pil -eq 1 ]]
		then
			sudo v4l2-ctl --device /dev/video0 --set-fmt-video=width=160,height=120
                elif [[ $pil -eq 2 ]]
                then
                        sudo v4l2-ctl --device /dev/video0 --set-fmt-video=width=352,height=288
                elif [[ $pil -eq 3 ]]
                then
                        sudo v4l2-ctl --device /dev/video0 --set-fmt-video=width=640,height=480
		fi



	elif [[ $pilihan -eq 4 ]]
	then
		v4l2-ctl -d /dev/video0 --all
	elif [[ $pilihan -eq 5 ]]
	then
		modinfo v4l2loopback
	fi
	echo ' '
	echo ' '
done
