# dadido-sysprog

<div align="center">
<h1>
    Linux Video Device Driver
</h1>

<h2>
   Kelompok Dadido
</h2>
</div>

Pada proyek ini, kami memanfaatkan beberapa device driver untuk mengontrol webcam. Adapun hal yang dapat dilakukan adalah merekam video, mengubah frame rate, dan mengukur besar frame. 

### Hal yang perlu dilakukan sebelumnya : 
- Clone repository ini : git clone https://gitlab.com/audillaputri/dadido-sysprog.git
- Install mencoder : sudo apt-get install -y mencoder.
- Install v4l2loopback : sudo apt-get install -y v4l2loopback-dkms.
- Install v4l2 utility : sudo apt-get install v4l-utils
- chmod u+x webcam_dadido.sh

Jika langkah di atas sudah dilakukan, maka jalankan webcam_dadido.sh

## Anggota 
1. Audilla Putri (1806186755)
2. Fauzan Pradana Linggih (1806205281)
3. Muhammad Fathurizki Herlando (1806205496)
